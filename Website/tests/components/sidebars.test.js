const path = require('path');
const sidebars = require(path.resolve(__dirname, '../../sidebars.js'));

describe('sidebars', () => {
  it('should have a tutorialSidebar property', () => {
    expect(sidebars).toHaveProperty('tutorialSidebar');
  });
});
